package co.com.goty.usecase;

import co.com.goty.model.gateways.BackendConsumer;
import co.com.goty.model.gateways.Product;
import co.com.goty.model.gateways.exception.SimilarsNotFoundException;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;


@RequiredArgsConstructor
public class ProductUseCase {

    private final BackendConsumer backendConsumer;

    public Flux<Product> getAllProducts(){
        return backendConsumer.getAllProducts();
    }

    public Flux<Product> getSimilars(Integer id){
        return backendConsumer.getSimilarProducts(id)
                .parallel()
                .runOn(Schedulers.parallel())
                .flatMap(this::getProduct)
                //Si tod0 el flux sale vacío, dispara un mensaje de aviso.
                .sequential()
                .switchIfEmpty(Mono.error(new SimilarsNotFoundException("No se encontraron similares.")));
    }

    private Mono<Product> getProduct(Integer productsId){
        return backendConsumer.getProduct(productsId)
                .onErrorResume(throwable -> Mono.empty());//Dar manejo al error y devolver vació.
    }
}
