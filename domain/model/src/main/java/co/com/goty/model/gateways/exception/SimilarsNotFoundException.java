package co.com.goty.model.gateways.exception;

public class SimilarsNotFoundException extends RuntimeException{
    public SimilarsNotFoundException(String message){
        super(message);
    }
}
