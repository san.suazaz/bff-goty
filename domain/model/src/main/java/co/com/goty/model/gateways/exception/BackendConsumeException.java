package co.com.goty.model.gateways.exception;

import lombok.Data;

@Data
public class BackendConsumeException extends RuntimeException {
    private String action;

    public BackendConsumeException(String message, RuntimeException ex, String action){
        super(message, ex);
        this.action = action;
    }

    public BackendConsumeException(String message){
        super(message);
    }
}
