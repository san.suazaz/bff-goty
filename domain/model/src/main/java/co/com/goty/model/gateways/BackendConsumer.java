package co.com.goty.model.gateways;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BackendConsumer {
    Flux<Product> getAllProducts();

    Mono<Product> getProduct(Integer id);

    Flux<Integer> getSimilarProducts(Integer id);
}
