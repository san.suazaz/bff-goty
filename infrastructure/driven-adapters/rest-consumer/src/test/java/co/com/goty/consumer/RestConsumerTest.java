package co.com.goty.consumer;

import co.com.goty.model.gateways.exception.BackendConsumeException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;
import java.io.IOException;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


class RestConsumerTest {

    private static BackendConsumerImpl restConsumer;

    private static MockWebServer mockBackEnd;


    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
        var webClient = WebClient.builder().baseUrl(mockBackEnd.url("/").toString()).build();
        restConsumer = new BackendConsumerImpl(webClient);
    }

    @AfterAll
    static void tearDown() throws IOException {

        mockBackEnd.shutdown();
    }

    @Test
    @DisplayName("Validate get similars.")
    void validateSimilarTest() {
        mockBackEnd.enqueue(new MockResponse()
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.OK.value())
                .setBody("[\n" +
                        "    2,\n" +
                        "    3,\n" +
                        "    4\n" +
                        "]"));
        var response = restConsumer.getSimilarProducts(1);

        StepVerifier.create(response)
                .expectNext(2, 3, 4)
                .expectComplete()
                .verify();
    }

    @Test
    @DisplayName("Get detail of product success")
    void getDetailProductSuccess() {

        mockBackEnd.enqueue(new MockResponse()
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.OK.value())
                .setBody("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"cyberpunk\",\n" +
                        "    \"url\": \"https://www.somosxbox.com/wp-content/uploads/2022/02/cri%CC%81ticas-a-Cyberpunk-2077-next-gen.jpg\",\n" +
                        "    \"price\": 10\n" +
                        "}"));
        var response = restConsumer.getProduct(1);

        StepVerifier.create(response)
                .expectNextMatches(Objects::nonNull)
                .verifyComplete();
    }

    @Test
    @DisplayName("Get detail of product timeOut Exception.")
    void getDetailProductTimeOut() {

        mockBackEnd.enqueue(new MockResponse()
                        .setBodyDelay(3000, TimeUnit.MILLISECONDS)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        var response = restConsumer.getProduct(100);

        StepVerifier.create(response)
                .expectError(BackendConsumeException.class)
                .verify(Duration.ofMillis(4000));
    }

    @Test
    @DisplayName("Internal server error test.")
    void getDetailProduct5xxTest(){
        mockBackEnd.enqueue(new MockResponse()
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        var response = restConsumer.getProduct(1000);

        StepVerifier.create(response)
                .expectError(BackendConsumeException.class)
                .verify(Duration.ofMillis(1000));
    }

    @Test
    @DisplayName("Get one product business error test.")
    void getDetailProduct4xxTest(){
        mockBackEnd.enqueue(new MockResponse()
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.NOT_FOUND.value()));

        var response = restConsumer.getProduct(5);

        StepVerifier.create(response)
                .expectError(BackendConsumeException.class)
                .verify(Duration.ofMillis(1000));
    }
}