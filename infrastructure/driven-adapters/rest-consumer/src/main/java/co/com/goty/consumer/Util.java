package co.com.goty.consumer;

import co.com.goty.model.gateways.Product;
import reactor.core.publisher.Flux;

public class Util {

    public static Product castToProduct(ProductResponse productResponse){
        Product product = new Product();
        product.setId(productResponse.getId());
        product.setName(productResponse.getName());
        product.setUrl(productResponse.getUrl());
        product.setPrice(productResponse.getPrice());
        return product;
    }
}
