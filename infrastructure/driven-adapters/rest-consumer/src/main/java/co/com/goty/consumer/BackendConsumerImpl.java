package co.com.goty.consumer;

import co.com.goty.model.gateways.BackendConsumer;
import co.com.goty.model.gateways.Product;
import co.com.goty.model.gateways.exception.BackendConsumeException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.TimeoutException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BackendConsumerImpl implements BackendConsumer {

    private static final Logger logger = LoggerFactory.getLogger(BackendConsumerImpl.class);
    private final WebClient client;

    @Override
    public Flux<Product> getAllProducts() {
        return getProductResponse().flatMap(productResponse -> Flux.just(Util.castToProduct(productResponse)));
    }

    @Override
    public Mono<Product> getProduct(Integer id) {
        Mono<ProductResponse> monoProductResponse = client
                .get()
                .uri("/product/" + id)
                .retrieve()
                .onStatus(HttpStatusCode::isError, clientResponse -> {
                    logger.error(String.format("Error obteniendo detalle de producto %d.", id));
                    return Mono.error(new BackendConsumeException("No se pudo obtener el detalle del producto. Backend Error"));
                })
                .bodyToMono(ProductResponse.class)
                .timeout(Duration.ofMillis(1000))
                .onErrorResume( TimeoutException.class, ex -> {
                    /*if (ex instanceof WebClientRequestException || ex instanceof ReadTimeoutException) {
                        logger.error("Se cierra la conexión por tiempo de espera.");
                        return Mono.error(new BackendConsumeException("Tiempo de espera superado", ex, "getProduct"));
                    }*/
                    logger.error("-----Ocurrió un error inesperado.---------" + ex.getCause().toString());
                    return Mono.error(new BackendConsumeException("Error inesperado", ex, "getProduct"));
                })
                .onErrorResume(ReadTimeoutException.class, error -> {
                    // Manejar el timeout de lectura aquí
                    System.err.println("Se produjo un timeout de lectura al conectarse al servidor.");
                    return Mono.empty(); // Puedes devolver un resultado vacío u otro valor predeterminado
                });
        return monoProductResponse.map(Util::castToProduct);
    }

    @Override
    public Flux<Integer> getSimilarProducts(Integer id) {
        return client
                .get()
                .uri("/product/" + id + "/similars")
                .retrieve()
                .bodyToMono(List.class)
                .flatMapMany(listProducts -> Flux.fromIterable(listProducts));
    }

    @CircuitBreaker(name = "testGet" /*, fallbackMethod = "testGetOk"*/)
    private Flux<ProductResponse> getProductResponse() {
        return client
                .get()
                .uri("/products/")
                .retrieve()
                .bodyToFlux(ProductResponse.class);
    }


// Possible fallback method
//    public Mono<String> testGetOk(Exception ignored) {
//        return client
//                .get() // TODO: change for another endpoint or destination
//                .retrieve()
//                .bodyToMono(String.class);
//    }

}
