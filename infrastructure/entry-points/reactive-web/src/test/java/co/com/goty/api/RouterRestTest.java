package co.com.goty.api;

import co.com.goty.model.gateways.Product;
import co.com.goty.usecase.ProductUseCase;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;


@ContextConfiguration(classes = {RouterRest.class, Handler.class})
@WebFluxTest
class RouterRestTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean( name = "productUseCase")
    private ProductUseCase productUseCase;

    @Test
    @DisplayName("Get all products success test.")
    void testListProducts() {
        Mockito.when(productUseCase.getAllProducts()).thenReturn(Flux.just(getProductMock()));
        webTestClient.get()
                .uri("/api/goty/products")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Product.class);
    }

    private Product getProductMock(){
        Product product = new Product();
        product.setPrice(1000);
        product.setId("1");
        product.setName("cyberpunk");

        return product;
    }
}
