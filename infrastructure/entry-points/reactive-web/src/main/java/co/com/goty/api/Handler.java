package co.com.goty.api;

import co.com.goty.usecase.ProductUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class Handler {

    private final ProductUseCase productUseCase;

    public Mono<ServerResponse> listProducts(ServerRequest serverRequest) {
        return productUseCase.getAllProducts()
                .collectList()
                .flatMap(products -> ServerResponse.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .bodyValue(products));
    }

    public Mono<ServerResponse> getSimilars(ServerRequest serverRequest) {
        return productUseCase.getSimilars(Integer.parseInt(serverRequest.pathVariable("id")))
                .collectList()
                .flatMap(similarProducts -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(similarProducts));
    }
}
